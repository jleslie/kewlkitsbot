import StringIO
import json
import logging
import random
import urllib
import urllib2

# for sending images
from PIL import Image
import multipart

# standard app engine imports
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
import webapp2

import requests
import json

TOKEN = '172581189:AAEd_z8CF1jbpqxRTORgY5zgN5lRVkc2HIY'

BASE_URL = 'https://api.telegram.org/bot' + TOKEN + '/'

# ================================

""" used to remove the unicode in an input """
def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def weatherInfo(city):
    openWeatherAPIkey = 'a05cbc337225e9eb9072679612de15af'
    url = 'http://api.openweathermap.org/data/2.5/weather?q='
    url += city
    url += '&APPID=' + openWeatherAPIkey
    response = requests.get(url)
    weatherStr = 'City: ' + city + '\n'
    weatherStr += byteify(response.json())['weather'][0]['description'].capitalize() + '\n'
    weatherStr += 'Current : ' + str(byteify(response.json())['main']['temp'] - 273) + 'C\n'
    weatherStr += 'Max Temp: ' + str(byteify(response.json())['main']['temp_max'] - 273) + 'C\n'
    weatherStr += 'Min Temp: ' + str(byteify(response.json())['main']['temp_min'] - 273) + 'C\n'
    weatherStr += 'Humidity: ' + str(byteify(response.json())['main']['humidity']) + '%\n'
    weatherStr += 'Wind Spd: ' + str(byteify(response.json())['wind']['speed'] * 1.61) + 'km/h'

    return weatherStr

# ================================

def randomBeerLookup():
    brewAPIKey = 'c84170522e3b07bdd5f71b2362eaa365'
    beerURL = 'http://api.brewerydb.com/v2/beer/random?key='
    beerURL += brewAPIKey
    response = requests.get(beerURL)

    beerStr = 'Beer name: ' + byteify(response.json())['data']['name'] + '\n'
    beerStr += 'ABV : ' + str(byteify(response.json())['data']['abv']) + '%\n'
    beerStr += 'Style: ' + str(byteify(response.json())['data']['style']['name']) + '\n'
    beerStr += 'Description: ' + byteify(response.json())['data']['description']

    return beerStr

# ================================

def whatsIn(food):
    nutAPPId = 'eba52b8c'
    nutAPPKey = 'f4e61dd6fab79fb3c7a5fae1a03ae3ca'
    nutURL = 'https://api.nutritionix.com/v1_1/search/'
    nutURL += food
    nutURL += '?fields=item_name%2Cbrand_name%2Cnf_calories%2Cnf_total_fat%2Cnf_total_carbohydrate%2Cnf_sugars%2Cnf_dietary_fiber%2Cnf_protein'
    nutURL += '&appId=' + nutAPPId
    nutURL += '&appKey=' + nutAPPKey
    response = requests.get(nutURL)

    nutStr = ''
    data = json.loads(response.text)
    
    hits = data['hits']
    print hits
    for items in hits:
        nutStr += 'Item name: ' + items['fields']['item_name'] + '\n'
        nutStr += 'Calories: ' + str(items['fields']['nf_calories']) + '\n'
        nutStr += 'Total fat: ' + str(items['fields']['nf_total_fat']) + '\n'
        nutStr += 'Total carbs: ' + str(items['fields']['nf_total_carbohydrate']) + '\n'
        nutStr += 'Sugars: ' + str(items['fields']['nf_sugars']) + '\n'
        nutStr += 'Dietary fiber: ' + str(items['fields']['nf_dietary_fiber']) + '\n'
        nutStr += 'Total protein: ' + str(items['fields']['nf_protein']) + '\n\n'
    return nutStr

# ================================

class EnableStatus(ndb.Model):
    # key name: str(chat_id)
    enabled = ndb.BooleanProperty(indexed=False, default=False)


# ================================

def setEnabled(chat_id, yes):
    es = EnableStatus.get_or_insert(str(chat_id))
    es.enabled = yes
    es.put()

def getEnabled(chat_id):
    es = EnableStatus.get_by_id(str(chat_id))
    if es:
        return es.enabled
    return False


# ================================

class MeHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getMe'))))


class GetUpdatesHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getUpdates'))))


class SetWebhookHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        url = self.request.get('url')
        if url:
            self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'setWebhook', urllib.urlencode({'url': url})))))


class WebhookHandler(webapp2.RequestHandler):
    def post(self):
        urlfetch.set_default_fetch_deadline(60)
        body = json.loads(self.request.body)
        logging.info('request body:')
        logging.info(body)
        self.response.write(json.dumps(body))

        update_id = body['update_id']
        try:
            message = body['message']
        except:
            message = body['edited_message']
        message_id = message.get('message_id')
        date = message.get('date')
        text = message.get('text')
        fr = message.get('from')
        chat = message['chat']
        chat_id = chat['id']

        if not text:
            logging.info('no text')
            return

        def reply(msg=None, img=None):
            if msg:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg.encode('utf-8'),
                    'disable_web_page_preview': 'true',
                    'reply_to_message_id': str(message_id),
                })).read()
            elif img:
                resp = multipart.post_multipart(BASE_URL + 'sendPhoto', [
                    ('chat_id', str(chat_id)),
                    ('reply_to_message_id', str(message_id)),
                ], [
                    ('photo', 'image.jpg', img),
                ])
            else:
                logging.error('no msg or img specified')
                resp = None

            logging.info('send response:')
            logging.info(resp)

        if text.startswith('/'):
            if text == '/start':
                reply('Bot enabled')
                setEnabled(chat_id, True)
            elif text == '/stop':
                reply('Bot disabled')
                setEnabled(chat_id, False)
            elif text == '/image':
                img = Image.new('RGB', (512, 512))
                base = random.randint(0, 16777216)
                pixels = [base+i*j for i in range(512) for j in range(512)]  # generate sample image
                img.putdata(pixels)
                output = StringIO.StringIO()
                img.save(output, 'JPEG')
                reply(img=output.getvalue())
            elif '/weather' in text:
                cmd = text.split()
                if(len(cmd) > 1):
                    cmd.remove('/weather')
                    newS = ' '.join(cmd)
                    reply(weatherInfo(newS))
            elif '/whatisin' in text:
                cmd = text.split()
                if(len(cmd) > 1):
                    cmd.remove('/whatisin')
                    newS = ' '.join(cmd)
                    reply(whatsIn(newS))
            else:
                reply('What command?')

        # CUSTOMIZE FROM HERE

        elif 'who are you' in text.lower():
            reply('kewlkitsbot, created by Jonathan K: https://bitbucket.org/jleslie/kewlkitsbot')
        elif 'what time' in text.lower():
            reply('look at the corner of your screen!')
        elif 'are you siri' in text.lower():
            reply('NO! I AM NOT SIRI! I AM SMARTER (a little)')
        elif 'give me a drink' in text.lower():
            reply(randomBeerLookup())
        else:
            if getEnabled(chat_id):
                reply('I got your message! (but I do not know how to answer)')
            else:
                logging.info('not enabled for chat_id {}'.format(chat_id))


app = webapp2.WSGIApplication([
    ('/me', MeHandler),
    ('/updates', GetUpdatesHandler),
    ('/set_webhook', SetWebhookHandler),
    ('/webhook', WebhookHandler),
], debug=True)
